#include <SPI.h>
#include <Ethernet.h>
#include <EthernetUdp.h>

// Enter a MAC address and IP address for your controller below.
// The IP address will be dependent on your local network:
byte mac[] = {0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED};   // enter mac address of your ethernet controller
IPAddress ip(192, 168, 1, 177);                      // enter local IP of your ethernet controller
unsigned int localPort = 9090;                       // enter local port to listen on

// define state pins
int state_pin2 = 2;
int state_pin3 = 3;

// buffers for receiving and sending data
char packetBuffer[UDP_TX_PACKET_MAX_SIZE];  //buffer to hold incoming packet,
char ReplyBuffer[] = "acknowledged";        // a string to send back

// An EthernetUDP instance to let us send and receive packets over UDP
EthernetUDP Udp;

void setup() {

  // start the Ethernet and UDP:
  Ethernet.begin(mac, ip);
  Udp.begin(localPort);

  pinMode(state_pin2, OUTPUT);
  pinMode(state_pin3, OUTPUT);
  digitalWrite(state_pin2, LOW);
  digitalWrite(state_pin3, LOW);

  Serial.begin(9600);
}

void loop() {

  // if there's data available, read a packet
  int packetSize = Udp.parsePacket();
  if (packetSize) {
    Serial.print("Received packet of size ");
    Serial.println(packetSize);
    Serial.print("From ");
    IPAddress remote = Udp.remoteIP();
    for (int i = 0; i < 4; i++) {
      Serial.print(remote[i], DEC);
      if (i < 3) {
        Serial.print(".");
      }
    }
    Serial.print(", port ");
    Serial.println(Udp.remotePort());

    // read the packet into packetBufffer
    Udp.read(packetBuffer, UDP_TX_PACKET_MAX_SIZE);
    Serial.println("Received Data: ");
    Serial.println(packetBuffer);

    String s_packetBuffer(packetBuffer);

    if (s_packetBuffer == "W0") {
      digitalWrite(state_pin2, LOW);
      digitalWrite(state_pin3, LOW);
    }
    if (s_packetBuffer == "W1") {
      digitalWrite(state_pin2, HIGH);
      digitalWrite(state_pin3, LOW);
    }
    if (s_packetBuffer == "W2") {
      digitalWrite(state_pin2, LOW);
      digitalWrite(state_pin3, HIGH);
    }

    Serial.print("State of pin2: ");
    Serial.println(digitalRead(state_pin2));
    Serial.print("State of pin3: ");
    Serial.println(digitalRead(state_pin3));
    Serial.println("**************************");

    // send a reply to the IP address and port that sent us the packet we received
    Udp.beginPacket(Udp.remoteIP(), Udp.remotePort());
    Udp.write(ReplyBuffer);
    Udp.endPacket();
  }
  delay(10);  
}

